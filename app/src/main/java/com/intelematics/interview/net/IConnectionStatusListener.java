package com.intelematics.interview.net;

/**
 * Created by venugopalraog on 11/22/15.
 */
public interface IConnectionStatusListener {
    void onNetworkDown(String error);
    void onResponseError(String error_content, String error_id);
    void onException(Exception error);
}
